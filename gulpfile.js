const gulp = require('gulp');
const less = require('gulp-less');
const path = require('path');
const concat = require('gulp-concat');
const minify = require('gulp-minify');
const cleanCSS = require('gulp-clean-css');

const CLIENT_PATH = path.resolve(__dirname, 'src/client');

const tasks = [];

gulp.task('images', function () {
  return gulp.src(path.resolve(CLIENT_PATH, 'images') + '/**/*')
    .pipe(gulp.dest('dist/images'));
});
tasks.push('images');

gulp.task('styles', function () {
  gulp.src(path.resolve(CLIENT_PATH, 'styles/app.less'))
    .pipe(less({
      paths: [path.resolve(CLIENT_PATH, 'styles')]
    }))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/styles'));
  
    gulp.src(path.resolve(CLIENT_PATH, 'styles/pdkadmin.less'))
    .pipe(less({
      paths: [path.resolve(CLIENT_PATH, 'styles')]
    }))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/styles'));
    
    gulp.src(path.resolve(CLIENT_PATH, 'styles/about.less'))
    .pipe(less({
      paths: [path.resolve(CLIENT_PATH, 'styles')]
    }))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/styles'));
    
    gulp.src(path.resolve(CLIENT_PATH, 'styles/post-amp.less'))
    .pipe(less({
      paths: [path.resolve(CLIENT_PATH, 'styles')]
    }))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/styles'));

  gulp.src(path.resolve(CLIENT_PATH, 'styles/*.css'))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/styles'));
});
tasks.push('styles');

gulp.task('js', function () {
  gulp.src([
    path.resolve(CLIENT_PATH, 'js/jquery.js'),
    path.resolve(CLIENT_PATH, 'js/jquery.cookie.js'),
    path.resolve(CLIENT_PATH, 'js/main.js'),
    path.resolve(CLIENT_PATH, 'js/api.js'),
    path.resolve(CLIENT_PATH, 'js/popup.js'),
    path.resolve(CLIENT_PATH, 'js/search.js'),
    path.resolve(CLIENT_PATH, 'js/studio.js'),
    path.resolve(CLIENT_PATH, 'js/posts.js'),
    path.resolve(CLIENT_PATH, 'js/telegraph.js'),
    path.resolve(CLIENT_PATH, 'js/tune.js'),
    path.resolve(CLIENT_PATH, 'js/night-mode.js'),
    path.resolve(CLIENT_PATH, 'js/hogan.js'),
    path.resolve(CLIENT_PATH, 'js/author-page.js'),
    path.resolve(CLIENT_PATH, 'js/app.js'),
    path.resolve(CLIENT_PATH, 'js/ui.js')
  ])
    .pipe(concat('app.js'))
    .pipe(minify({
      ext:{
          src:'-dev.js',
          min:'.js'
      }
    }))
    .pipe(gulp.dest('dist/js'));

  gulp.src(path.resolve(CLIENT_PATH, 'js/pdkadmin.js'))
    .pipe(gulp.dest('dist/js'))
  
    gulp.src([
      path.resolve(CLIENT_PATH, 'js/jquery.js'),
      path.resolve(CLIENT_PATH, 'js/about.js')
    ])
    .pipe(concat('about.js'))
    .pipe(gulp.dest('dist/js'))
});
tasks.push('js');

if (process.env.NODE_ENV === 'development') {
  gulp.watch(path.resolve(CLIENT_PATH, 'styles/**/*'), ['styles']);
  gulp.watch(path.resolve(CLIENT_PATH, 'images/**/*'), ['images']);
  gulp.watch(path.resolve(CLIENT_PATH, 'js/**/*'), ['js']);
}

gulp.task('default',  tasks);