const $fs = require('fs');
const $path = require('path');
const $hogan = require('hogan.js');

const Timestamp = (new Date()).getTime();
const ENV = process.env.NODE_ENV;

class Templates {
  static get PATH () {
    return $path.resolve(__dirname, '../templates');
  }

  static get CACHE () {
    if (!global.TEMPLATES_CACHE) {
      global.TEMPLATES_CACHE = {};
    }

    return global.TEMPLATES_CACHE;
  }

  static setCache () {
    const files = $fs.readdirSync(Templates.PATH);
    
    files.reduce((r, file) => {
      const data = $fs.readFileSync($path.resolve(Templates.PATH, file), {
        encoding: 'utf8'
      });
      const name = file.replace(/\.html$/, '');

      r[name] = $hogan.compile(data);

      return r;
    }, Templates.CACHE);
  }

  static get (name) {
    return Templates.CACHE[name];
  }

  static render (name, options = {}) {
    if (!options.timestamp) {
      options.timestamp = Timestamp;
    }

    options.env_dev = ENV === 'development';
    options.env_prod = ENV !== 'development';
    options.notPost = !options.postData;
    
    return Templates.get(name).render(options);
  }
}

module.exports = Templates;