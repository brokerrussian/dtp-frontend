const $request = require('request');
const $Promise = require('bluebird');
const $config = require('config');
const _ = require('lodash');

class Admin {
  static get SERVER () {
    return $config.get('services.admin.url') + '/';
  }

  static async query (method, params) {
    const query = await new $Promise((resolve, reject) => {
      $request({
        method: 'post',
        url: Admin.SERVER + method,
        json: params || true
      }, (err, response, body) => {
        if (err) {
          return reject(err);
        }

        resolve(body);
      });
    });

    return query;
  }
}

module.exports = Admin;