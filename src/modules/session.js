const $mongo = require('../libs/mongo');

class Session {
  static get db () {
    return $mongo.collection('sessions');
  }

  static async get (sessionid) {
    const response = await Session.db.findOne({ sessionid });

    if (!response) {
      return;
    }

    return response.data;
  }

  static async set (sessionid, data) {
    const last = await Session.get(sessionid);
    let newData;

    if (!last) {
      await Session.db.insert({ sessionid, data });
    } else {
      newData = Object.assign(last, data);
      
      await Session.db.editOne({ sessionid }, { data: newData });
    }

    return last;
  }

  static async delete (sessionid) {
    await Session.db.deleteMany({ sessionid });
  }
}

module.exports = Session;