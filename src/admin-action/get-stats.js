const $Admin = require('../modules/admin-action');

module.exports = async function (req) {
  const stats = await $Admin.query('stats');

  return stats;
};