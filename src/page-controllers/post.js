const $Templates = require('../modules/templates');
const $API = require('../modules/api');
const $request = require('request');
const $config = require('config');
const $moment = require('moment');
const $fs = require('fs');
const $Promise = require('bluebird');
const $path = require('path');
const $qs = require('qs');

const _ = require('lodash');

const publisherBot = $config.get('services.publisherBot.username');
const filesService = $config.get('services.files.url');
const cgnodeService = $config.get('services.cgnode.url');
const DTPHost = $config.get('services.dtp.url');

const contentTelegraph = require('../functions/telegraph-content-replace').contentTelegraph;

async function setView (link) {
  $request(link);
}

let AMPStyleCache = false;
async function getAMPStyle () {
  if (AMPStyleCache) {
    return AMPStyleCache;
  }

  const style = await new $Promise((resolve, reject) => {
    $fs.readFile($path.resolve(__dirname, '../../dist/styles/post-amp.css'), 'utf8', (err, data) => {
      if (err) {
        return reject(err);
      }

      resolve(data);
    });
  });

  AMPStyleCache = style;

  return style;
}

function getFirstImage (content) {
  let img;

  for (const node of content) {
    if (node.children) {
      const resChild = getFirstImage(node.children);

      if (resChild && !img) {
        img = resChild;
      }
    }

    if (node.tag !== 'img') {
      continue;
    }

    if (img) {
      continue;
    }

    let url = node.attrs.src;

    if (url.match(/^\/(files)?/)) {
      url = contentTelegraph(url);
    }

    img = url;
  }

  return img;
}

function nodeDomToHTML (content) {
  const closedTags = ['iframe'];
  const telegraphLinkRegExp = /^(http(s)?:\/\/)?(telegra\.ph|graph\.org)?\/(.+)$/;

  return content.reduce((html, node) => {
    if (typeof node === 'string') {
      html += node;
      
      return html;
    }
      
    let tag = '';
    
    tag += '<' + node.tag;

    if (node.tag === 'pre') {
      if (!node.attrs) {
        node.attrs = {};
      }

      node.attrs.spellcheck = 'false';
    } else if (node.tag === 'a' && node.attrs.href.match(telegraphLinkRegExp)) {
      const gtLink = DTPHost + '/gt/' + node.attrs.href.match(telegraphLinkRegExp).pop();
      node.attrs['ivlink'] = `https://t.me/iv/url?url=${encodeURIComponent(gtLink)}&rhash=7c83b6ab2dc887`;
    } else if (node.tag === 'iframe' && node.attrs.src.match(/\/embed\//)) {
      const embedURL = node.attrs.src.match(/\/embed\/(.+)\?url=(.+)/).pop();

      node.attrs.embedorig = decodeURIComponent(embedURL);
    }
  
    if (node.attrs) {
      tag += Object.keys(node.attrs).map((name) => {
        let attrValue = node.attrs[name];
        const telegraphSourceRegExp = /^(http(s)?:\/\/(telegra\.ph|graph.org))?\/(file)?/;
        const telegraphFileRegExp = /^(http(s)?:\/\/(telegra\.ph|graph.org))?\/file/;
        
        if (name === 'src' && attrValue.match(telegraphSourceRegExp)) {
          if (attrValue.match(telegraphFileRegExp)) {
            attrValue = contentTelegraph(attrValue);
          } else {
            attrValue = '/fsp' + attrValue;
          }
        } else if (name === 'href' && attrValue.match(telegraphLinkRegExp)) {
          attrValue = '/gt/' + attrValue.match(telegraphLinkRegExp).pop();
        }
        
        return ' ' + name + '="' + attrValue + '"';
      });
    }

    let needClose;

    if (node.children || closedTags.includes(node.tag)) {
      needClose = true;
    }
    
    tag += needClose ? '>' + ( node.children ? nodeDomToHTML(node.children) : '' ) + '</' + node.tag + '>' : '/>';
    
    html += tag;
    
    return html;
  }, '');
}

module.exports = async function (params) {
  const { shortId, ampMode } = params;

  const Post = await $API.query('getPost/' + shortId)
    .then((response) => {
      if (!response.ok) {
        if (_.get(response, 'error.code') === 3) {
          throw 'PAGE_NOT_FOUND';
        }

        throw response;
      }

      return response.result;
    });

  const momentDate = $moment.unix(Post.date);
  const publishedDate = momentDate.format('MMMM DD, YYYY');

  const host = $config.get('services.dtp.url');
  const ampURL = host + '/amp/' + shortId;
  const originURL = host + '/' + shortId;
  const content = nodeDomToHTML(Post.content);

  const pagePost = $Templates.render('page-post', {
    shortId,
    url: Post.preview.url,
    title: Post.preview.title,
    content: content,
    noTelegramID: !Boolean(params.cookies.dtptoken),
    isTelegramID: Boolean(params.cookies.dtptoken),
    author_name: Post.preview.author_name,
    author_url: Post.preview.author_url,
    publishedDate,
    publisherBot,
    noAMP: !ampMode
  });

  let basePage, AMPStyle;

  if (ampMode) {
    basePage = 'page-amp-base';
    AMPStyle = await getAMPStyle();
  } else {
    basePage = 'page-base';
  }

  const postDataObj = Object.assign({}, Post.preview, {
    date: Post.date,
    tags: Post.tags,
    language: Post.language
  });
  const postData = Object.keys(postDataObj).reduce((r, key) => {
    let value = postDataObj[key];
    const noSkip = ['date', 'tags'];

    if (typeof value !== 'string' && !noSkip.includes(key)) {
      return r;
    }

    switch (key) {
      case 'date': 
        r[key] = (new Date(value * 1000)).toISOString();
      break;
      case 'tags': 
        r['keywords'] = value.join(', ');
      break;
      default: 
        r[key] = value.replace(/\n/g, ' ').replace(/"/g, '\'');
      break;
    }

    return r;
  }, {});

  let cover = getFirstImage(Post.content);

  if (cover) {
    cover = contentTelegraph(cover);

    const qs = $qs.stringify({
      title: postData.title,
      image: cover
    });
    const cover_url = cgnodeService + '/get?' + qs;

    postData.iv_cover = cover_url;

    if (!postData.image_url) {
      postData.image_url = cover_url;
    } else {
      postData.image_url = contentTelegraph(postData.image_url);
    }
  }

  const base = $Templates.render(basePage, {
    title: Post.preview.title,
    page: pagePost,
    AMPStyle,
    ampURL,
    originURL,
    postData,
    nightMode: params.cookies['n-m'] || false
  });

  setView(Post.preview.url);

  return base;
};