const $Templates = require('../modules/templates');
const $API = require('../modules/api');

const _ = require('lodash');

module.exports = async function (params) {
  const page = $Templates.render('page-about');

  return page;
};