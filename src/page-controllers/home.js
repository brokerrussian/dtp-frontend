const $Templates = require('../modules/templates');
const $API = require('../modules/api');
const $config = require('config');
const $Promise = require('bluebird');
const _ = require('lodash');

const contentTelegraph = require('../functions/telegraph-content-replace').contentTelegraph;

const publisherBotUsername = $config.get('services.publisherBot.username');

module.exports = async function (params) {
  const headerParams = {};

  if (params.session) {
    const user = params.session.user;
    headerParams.auth = true;
    
    if (user.photo) {
      const photo =  user.photo[0].file_id;
      headerParams.photo = $config.get('services.files.url') + '/photo/' + photo;
    } else {
      headerParams.symbolPhoto = user.first_name[0];
    }
  } else {
    headerParams.noAuth = true;
  }

  const header = $Templates.render('component-header', headerParams);

  const trendQuery = {};

  if (params.cookies.dtptoken) {
    trendQuery.token = params.cookies.dtptoken;
  }

  const qOffset = _.get(params, 'query.offset', 0);

  trendQuery.offset = Number(qOffset);

  const {
    trending,
    latest
  } = await $Promise.props({
    trending: $API.query('getTrending', trendQuery)
      .then((result) => {
        if (!result.ok) {
          throw result;
        }

        return result.result;
      }),
    latest: $API.query('getLatest', {
      token: trendQuery.token
    })
      .then((result) => {
        if (!result.ok) {
          throw result;
        }

        return result.result;
      })
  });

  async function checkNext () {
    trendQuery.offset += 10;

    const list = await $API.query('getTrending', trendQuery)
      .then((result) => {
        if (!result.ok) {
          throw result;
        }

        return result.result.posts;
      });

    if (list.length > 0) {
      return true;
    }

    return false;
  }

  let posts, pagination, latestPosts;

  if (trending.posts.length !== 0) {
    posts = $Templates.render('component-posts-list', {
      posts: trending.posts.map((post) => {
        if (post.preview.views > 1000) {
          post.preview.views = `${Math.round(post.preview.views / 1000)}k`;
        }

        if (post.preview.image_url) {
          post.preview.image_url = contentTelegraph(post.preview.image_url);
        }
  
        return post;
      })
    });
    
    const pPrev = Number(_.get(params, 'query.offset', 0)) - 10;
    const pNext = Number(_.get(params, 'query.offset', 0)) + 10;
    let needNext;

    if (trending.posts.length > 9) {
      needNext = await checkNext();
    }

    pagination = $Templates.render('component-pagination', {
      prev: pPrev >= 0 ? pPrev === 0 ? '0' : pPrev : false,
      next: needNext ? pNext : false,
      isLink: true
    });
  } else {
    posts = $Templates.render('component-empty-posts');
    pagination = '';
  }

  latestPosts = $Templates.render('component-posts-list-latest', {
    posts: latest.map((post) => {
      if (post.preview.views > 1000) {
        post.preview.views = `${Math.round(post.preview.views / 1000)}k`;
      }

      return post;
    })
  });

  const page = $Templates.render('page-home', {
    header,
    posts,
    latest: latestPosts,
    tags: trending.tags,
    popup_studio_landing: $Templates.render('popup-studio-landing', {
      publisherBotUsername
    }),
    popup_search_input: $Templates.render('popup-search-input'),
    isTelegramID: !!params.session,
    user: params.session && params.session.user,
    pagination,
    isTrending: trending.posts.length !== 0
  });
  
  const base = $Templates.render('page-base', {
    title: 'DTP — Telegraph Platform',
    page,
    nightMode: params.cookies['n-m'] || false
  });

  return base;
};