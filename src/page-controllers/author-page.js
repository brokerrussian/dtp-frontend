const $Templates = require('../modules/templates');
const $API = require('../modules/api');

const _ = require('lodash');

module.exports = async function (params) {
  const author = await $API.query('getAuthor/' + params.username)
    .then((data) => {
      if (!data.ok) {
        if (data.error.code === 6) {
          throw 'INVALID_USERNAME';
        } else if (data.error.code === 7) {
          throw 'INVALID_USERNAME';
        }

        throw data;
      }

      return data.result;
    });

  const page = $Templates.render('page-author', {
    author_name: author.name,
    username: params.username
  });

  const base = $Templates.render('page-base', {
    title: author.name || 'Author',
    page
  });

  return base;
};