var Posts = {};

Posts.OKRateHTML = 'Thanks! <a href="/share/{id}" target="_blank">Do Share?</a>';

Posts.rate = function (shortId, delta) {
  $('#rate #title').html(App.spinner);

  API.query('ratePost', {
    shortId: shortId,
    delta: delta
  })
    .then(function (response) {
      if (!response.ok) {
        throw response;
      }

      if (response.ok === true) {
        $('#rate .rate-button').hide();
        $('#rate #title').html(Posts.OKRateHTML.replace('{id}', shortId));
      }
    })
    .catch(function (err) {
      $('#rate #title').html('Server Error');
      console.error(err);
    });
};