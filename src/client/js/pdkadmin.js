$(document).ready(function () {
  Admin.openPage('delete-content');
});

var Admin = {};

Admin.openPage = function (id) {
  $('#pages .page').hide();
  $('#pages .default').hide();
  $('#pages .page#' + id).show();

  switch (id) {
    case 'stats': 
      Admin.getStats();
    break;
  }
};

Admin.getStats = function () {
  Admin.action('get-stats')
    .then(function (data) {
      Object.keys(data).forEach(function (group) {
        Object.keys(data[group]).forEach(function (interval) {
          $('span#' + group + '-' + interval).html(data[group][interval]);
        });
      });
    })
    .catch(function (err) {
      console.error(err);
      alert('Error!');
    })
};

Admin.action = function (action, params) {
  if (!params) {
    params = {};
  }

  return new Promise(function (resolve, reject) {
    $.ajax({
      method: 'post',
      url: '/ac/' + action,
      data: JSON.stringify(params),
      dataType: 'json',
      contentType: 'application/json',
      success: resolve,
      error: reject
    });
  });
};

Admin.deletePost = function (blockAuthor) {
  var linkInput = $('input#dtp-post-link');
  var reasonInput = $('textarea#block-reason');
  var link = linkInput.val();
  var reason = reasonInput.val();
  var path = link.split(window.location.hostname + '/')[1];

  if (!path) {
    return alert('Invalid link');
  }

  if (!reason) {
    return alert('Invalid reason!');
  }

  Admin.action('delete-post', {
    path: path,
    reason: reason,
    blockAuthor: Boolean(blockAuthor)
  })
  .then(function (data) {
    if (data.ok) {
      linkInput.val('');
      reasonInput.val('');

      alert('Done!');
    }
  })
  .catch(function (err) {
    console.error(err);
    alert('Error!');
  })
};