var Popup = {};

Popup.layouts = [];
Popup.repositionIntervals = {};

Popup.reposition = function (id) {
  var blacklist = ['popup-search-input'];

  if (blacklist.indexOf(id) >= 0) {
    return;
  }

  var windowHeight = window.innerHeight;
  var box = $('.popup-window#' + id + ' #box');
  var boxHeight = box.height() + 50;

  if (boxHeight > windowHeight) {
    box.css({
      top: '15px'
    });

    return;
  }

  var top = Math.round((Number(windowHeight) - Number(boxHeight)) / 3);

  // console.log({
  //   windowHeight,
  //   boxHeight,
  //   top
  // });

  box.css({
    top: top + 'px'
  });

  if (!Popup.repositionIntervals[id]) {
    Popup.repositionIntervals[id] = setInterval(function (){ 
      Popup.reposition(id);
    }, 250);
  }
};

Popup.hideLast = function () {
  const lastPopup = Object.assign([], Popup.layouts).pop();
    
  $('.popup-window#' + lastPopup + ' #shadow').hide();
};

Popup.showLast = function () {
  const lastPopup = Object.assign([], Popup.layouts).pop();
    
  $('.popup-window#' + lastPopup + ' #shadow').show();
};

Popup.open = function (id, layout) {
  if (!layout) {
    Popup.close();
  } else {
    Popup.hideLast();
  }

  Popup.layouts.push(id);

  $('html, body').css({
    overflow: 'hidden'
  });
  $('.popup-window#' + id).show().animate({ scrollTop: 0 }, 0);

  switch (id) {
    case 'popup-search-input':
      $('#popup-search-input #query').focus();
    break;
    case 'popup-import-telegraph':
      CStudio.saveAccountHandler = CStudio.createAccount;
      $('#popup-import-telegraph #token-value').val('');
    break;
    case 'popup-author-url': 
      CStudio.setAuthorNameValue.val('').focus();
      CStudio.setAuthorNameView.find('span').html('');
      CStudio.setAuthorNameView.removeClass('yes no');
    break;
  }

  Popup.reposition(id);
};

Popup.close = function (layoutId) {
  if (layoutId) {
    $('.popup-window#' + layoutId).hide();
    clearInterval(Popup.repositionIntervals[layoutId]);
    Popup.repositionIntervals[layoutId] = false;
  } else {
    $('html, body').css({
      overflow: 'auto'
    });
    $('.popup-window').hide();
  }

  Popup.layouts = Popup.layouts.filter(function (_id) {
    return !(_id === layoutId);
  });
  Popup.showLast();
};