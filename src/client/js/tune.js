$(document).ready(function () {
  if (window.location.pathname === '/tune') {
    Tune.loadFilter();
  }
});

var Tune = {};

Tune.filter = {
  language: [],
  tags: []
};

Tune.box = $('#page-tune #box');
Tune.spinner = $('#page-tune #spinner');
Tune.languageBox = Tune.box.find('#language-box #content');
Tune.keywordsBox = Tune.box.find('#keywords-box #content #words');

Tune.loadFilter = function () {
  API.query('getTrendingFilter')
    .then(function (response) {
      if (!response.ok) {
        throw response;
      }

      Tune.filter = response.result;

      Tune.renderFilter();
    })
    .catch(function (err) {
      console.error(err);
      alert('Server Error!');
    })
};

Tune.renderFilter = function () {
  Tune.languageBox.find('.lang').removeClass('hover');

  if (Tune.filter.language.length === 0) {
    Tune.languageBox.find('#lang-all').addClass('hover');
  } else {
    Tune.filter.language.forEach(function (lang) {
      Tune.languageBox.find('#lang-' + lang).addClass('hover');
    });
  }


  const WordTemplate = Hogan.compile('<div class="word" onclick="Tune.deleteWord(this)">{{w}}</div>');

  Tune.keywordsBox.html(Tune.filter.tags.reduce(function (r, w) {
    r += WordTemplate.render({ w: w });

    return r;
  }, ''));
};

Tune.setLanguage = function (id) {
  const lang = id.split('lang-').pop();

  if (lang === 'all') {
    Tune.filter.language = [];
    Tune.renderFilter();
    Tune.save();

    return;
  }

  if (Tune.filter.language.indexOf(lang) >= 0) {
    Tune.filter.language = Tune.filter.language.filter(function (l) {
      return l !== lang;
    });
  } else {
    Tune.filter.language.push(lang);
  }

  Tune.renderFilter();
  Tune.save();
};

Tune.addWord = function (input) {
  const wordRegExp = /#?([а-яА-Я\w0-9])+/g;
  const text = input.value;
  const words = text.match(wordRegExp);

  if (( Tune.filter.tags.length + words.length ) > 10) {
    return alert('Words are too much!');
  }

  words.forEach(function (w) {
    if (!Tune.filter.tags.find((item) => item.toLowerCase() === w.toLowerCase())) {
      Tune.filter.tags.push(w);
    }
  });

  input.value = '';
  input.blur();

  Tune.renderFilter();
  Tune.save();
  Tune.box.find('#words-input').focus();
 };

Tune.save = function () {
  API.query('setTrendingFilter', Tune.filter)
  .then(function (response) {
    if (!response.ok) {
      throw response;
    }
  })
  .catch(function (err) {
    console.error(err);
    alert('Server Error!');
  })
};

Tune.deleteWord = function (obj) {
  const word = $(obj).html();

  Tune.filter.tags = Tune.filter.tags.filter(function (w) {
    return !(w === word);
  });

  Tune.renderFilter();
  Tune.save();
}