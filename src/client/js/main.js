$(document).ready(function () {
  App.init();

  $('iframe').wrap('<div class="iframe_helper"></div>');
  $('.iframe_helper').wrap('<div class="iframe_wrap"></div>');

  $('figcaption').each(function () {
    const elem = $(this)
    const text = elem.text();

    if (!text) {
      elem.remove();
    }
  });
});

const setYTIF = {};

function _resizeIframe(iframeWindow, width, height) {
  const iURL = iframeWindow.document.URL;
  let ignoreHeight;
  if (iURL.match(/youtube/)) {
    if (setYTIF[iURL]) {
      ignoreHeight = true;
    } else {
      setYTIF[iURL] = true;
    }
  }

  $('iframe').map(function() {
    let thisWindow = null;

    try {
      thisWindow = this.contentWindow;
    } catch (e) {}
    if (thisWindow && thisWindow == iframeWindow) {
      let ratio = height / width;

      if (!ignoreHeight) {
        this.setAttribute('width',  '640');
        this.setAttribute('height', Math.round(640 * ratio) + '');      
      }


      if (this.parentNode && this.parentNode.classList.contains('iframe_helper')) {
        this.parentNode.style.paddingTop = (ratio * 100) + '%';
      }
    }
  });
}

var App = {};

App.init = function () {
  console.log('Page ready');

  $(window).keydown(function (event) {
    if (event.key === 'Escape') {
      var layouts = Object.assign([], Popup.layouts);
      var lastLayout = layouts.pop();

      $('.popup-window#' + lastLayout + ' #shadow').click();

      clearInterval(Popup.repositionIntervals[lastLayout]);
      Popup.repositionIntervals[lastLayout] = false;
    }
  });

  App.initScrollNavBar();
};

App.spinner = '<div id="followingBallsG">' +
  '<div id="followingBallsG_1" class="followingBallsG"></div>' +
  '<div id="followingBallsG_2" class="followingBallsG"></div>' +  
  '<div id="followingBallsG_3" class="followingBallsG"></div>' +  
  '<div id="followingBallsG_4" class="followingBallsG"></div>' +  
  '</div>';  

App.initScrollNavBar = function () {
  var pageWidth = $('.main-container').width();
  var windowWidth = $(window).width();

  if (windowWidth > pageWidth) {
    var buttWidth = Math.round((windowWidth - pageWidth) / 2);
    $('#scroll-navbar .butt').css({width: buttWidth});
  }

  function goUp () {
    $("html, body").animate({ scrollTop: 0 }, "slow");
  }

  function goDown () {
    $("html, body").animate({ scrollTop: $(document).height() }, "slow");
  }

  $('#scroll-navbar .up').click(goUp);
  $('#scroll-navbar .down').click(goDown);
};