const $config = require('config');

const filesHost = $config.get('services.files.url');

function contentTelegraph (text) {
  const contentRegExp = /^(http(s)?:\/\/telegra\.ph)?\/file\/(.+)/g;
  const match = text.match(contentRegExp);

  if (!text) {
    return text;
  } else if (!match) {
    return text;
  }

  return text.replace(contentRegExp, filesHost + '/content/file/$3');
}


module.exports = {
  contentTelegraph
};