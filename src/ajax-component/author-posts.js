const $API = require('../modules/api');
const $Templates = require('../modules/templates');
const _ = require('lodash');

const contentTelegraph = require('../functions/telegraph-content-replace').contentTelegraph;

module.exports = async function (req) {
  const offset = Number(req.body.offset) || 0;
  const query = req.body.query ? String(req.body.query) : false;

  if (offset < 0) {
    return 'offset is invalid';
  }

  const queryParams = {
    offset
  };

  if (query) {
    queryParams.query = query;
  }
  
  const list = await $API.query('getAuthorPosts/' + req.body.username, queryParams);

  if (!list.ok) {
    throw list;
  }

  let html, pagination = '';

  if (list.result.length !== 0) {
    html = $Templates.render('component-posts-list', {
      posts: list.result.map((post) => {
        if (post.preview.views > 1000) {
          post.preview.views = `${Math.round(post.preview.views / 1000)}k`;
        }

        if (post.preview.image_url) {
          post.preview.image_url = contentTelegraph(post.preview.image_url);
        }
  
        return post;
      })
    });

    const pPrev = Number(offset) - 15;
    const pNext = Number(offset) + 15;

    pagination = $Templates.render('component-pagination', {
      prev: pPrev >= 0 ? pPrev === 0 ? '0' : pPrev : false,
      next: list.result.length > 14 ? pNext : false,
      isJS: 'AuthorPage.setPostsOffset'
    });
  } else {
    html = $Templates.render('component-empty-posts');
  }

  return {
    ok: true,
    html,
    pagination
  };
};