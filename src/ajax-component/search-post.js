const $API = require('../modules/api');
const $Templates = require('../modules/templates');

const contentTelegraph = require('../functions/telegraph-content-replace').contentTelegraph;

module.exports = async function (req) {
  const query = req.body.query;
  const tags = req.body.tags;
  const offset = Number(req.body.offset) || 0;

  if (!query && !tags) {
    return 'query/tags is not found';
  } else if (offset < 0) {
    return 'offset is invalid';
  }

  const searchParams = {
    query,
    tags,
    offset
  };

  const list = await $API.query('searchPost', searchParams);

  if (!list.ok) {
    throw list;
  }

  let html, pagination = '';

  async function checkNext () {
    searchParams.offset += 10;

    const list = await $API.query('searchPost', searchParams);

    if (list.result.length > 0) {
      return true;
    }

    return false;
  }

  if (list.result.length !== 0) {
    html = $Templates.render('component-posts-list', {
      posts: list.result.map((post) => {
        if (post.preview.views > 1000) {
          post.preview.views = `${Math.round(post.preview.views / 1000)}k`;
        }

        if (post.preview.image_url) {
          post.preview.image_url = contentTelegraph(post.preview.image_url);
        }

        return post;
      })
    });

    const pPrev = Number(offset) - 10;
    const pNext = Number(offset) + 10;
    let needNext;

    if (list.result.length > 9) {
      needNext = await checkNext();
    }

    pagination = $Templates.render('component-pagination', {
      prev: pPrev >= 0 ? pPrev === 0 ? '0' : pPrev : false,
      next: needNext ? pNext : false,
      isJS: 'Search.setResultOffset'
    });
  } else {
    html = $Templates.render('component-empty-posts');
  }

  return {
    ok: true,
    html,
    pagination
  };
};
