const $API = require('../modules/api');
const $Templates = require('../modules/templates');

module.exports = async function (req) {
  const token = req.cookies.dtptoken;

  if (!token) {
    return 'invalid token';
  }
  
  const accounts = await $API.query('getAccounts', {
    token
  });

  if (!accounts.ok) {
    throw accounts;
  }

  let html;

  if (accounts.result.length !== 0) {
    html = $Templates.render('component-group-account-list', {
      list: accounts.result.map((item) => {
        item.short_name = item.short_name.replace(/'/g, '’');
        
        return item;
      })
    });
  } else {
    html = '';
  }

  return {
    ok: true,
    html,
    json: accounts.result
  };
};