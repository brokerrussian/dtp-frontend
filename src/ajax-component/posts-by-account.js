const $API = require('../modules/api');
const $Templates = require('../modules/templates');
const _ = require('lodash');

module.exports = async function (req) {
  const token = req.cookies.dtptoken;

  if (!token) {
    return 'invalid token';
  }

  const query = req.body.query;
  const offset = Number(req.body.offset) || 0;

  if (offset < 0) {
    return 'offset is invalid';
  }

  const telegraph = req.body.telegraph || false;

  const getParams = {
    query,
    offset,
    telegraph,
    token
  };
  
  const list = await $API.query('getPostsByAccount/' + req.body.account_id, getParams);

  if (!list.ok) {
    throw list;
  }

  async function checkNext () {
    getParams.offset += 15;

    const list = await $API.query('getPostsByAccount/' + req.body.account_id, getParams);

    if (list.result.length > 0) {
      return true;
    }

    return false;
  }

  let html, pagination = '';

  if (list.result.length !== 0) {
    let component;

    if (!telegraph) {
      component = 'component-account-posts';
    } else {
      component = 'component-account-posts-telegraph';
    }
    
    html = $Templates.render(component, {
      list: list.result.map((post) => {
        if (post.preview.views > 1000) {
          post.preview.views = `${Math.round(post.preview.views / 1000)}k`;
        }

        post.preview.title = post.preview.title.replace(/'/g, '’');

        if (!post.short_id) {
          post.nopublished = true;
        }
  
        return post;
      })
    });

    const pPrev = Number(offset) - 15;
    const pNext = Number(offset) + 15;
    let needNext;

    if (list.result.length > 14) {
      needNext = await checkNext();
    }

    pagination = $Templates.render('component-pagination', {
      prev: pPrev >= 0 ? pPrev === 0 ? '0' : pPrev : false,
      next: needNext ? pNext : false,
      isJS: 'CStudio.setAccountPostsOffset'
    });
  } else {
    html = $Templates.render('component-empty-posts');
  }

  return {
    ok: true,
    html,
    pagination
  };
};