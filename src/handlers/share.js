const $config = require('config');
const bot = $config.get('services.publisherBot.username');

module.exports = function (app) {
  app.get('/share/:id', (req, res) => {
    const id = req.params.id;
    const shareURL = 'http://t.me/' + bot + '/?start=share' + id;

    res.redirect(shareURL);
  });
};