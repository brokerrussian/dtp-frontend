const $API = require('../modules/api');
const $log = require('../libs/log');
const $Session = require('../modules/session');
const _ = require('lodash');

module.exports = function (app) {
  app.get('/auth/*', (req, res) => {
    const openToken = req.params[0];

    $API.query('getToken/' + openToken)
      .then(async (response) => {
        if (!response.ok) {
          if (_.get(response, 'error.code') === 4) {
            throw 'INVALID_OPEN_TOKEN';
          } else {
            throw response;
          }
        }

        const secretToken = response.result.secretToken;
        const user = _.pick(response.result, ['id', 'first_name', 'last_name', 'photo']);

        await $Session.set(secretToken, {
          user
        });

        res.cookie('dtptoken', secretToken, { maxAge: 2678400000 });
        res.redirect('/');
      })
      .catch((err) => {
        if (err === 'INVALID_OPEN_TOKEN') {
          return res.end('Invalid open token!');
        }

        $log.error(err);
        res.end('Server Error!');
      });
  });
};