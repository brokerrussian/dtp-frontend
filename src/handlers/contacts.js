const $Pages = require('../modules/pages');

module.exports = function (app) {
  app.get('/contacts', (req, res) => {
    $Pages.render('contacts')
      .then((data) => {
        res.end(data);
      })
      .catch((err) => {
        console.log(err);
        res.status(502).end('Unknown error...');
      })
  });
};