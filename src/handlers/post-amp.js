const $Pages = require('../modules/pages');

module.exports = function (app) {
  app.get('/amp/:shortId', (req, res) => {
    const shortId = req.params.shortId;

    res.writeHead(200, {
      'Cache-Control': 'max-age=3600'
    });

    $Pages.render('post', {
      ampMode: true,
      shortId,
      cookies: req.cookies
    })
      .then((data) => {
        res.end(data);
      })
      .catch((err) => {
        console.log(err);
        res.status(502).end('Unknown error...');
      })
  });
};