const $API = require('../modules/api');
const $Pages = require('../modules/pages');
const _ = require('lodash');

module.exports = function (app) {
  app.get('/gt/:path', (req, res) => {
    const path = req.params.path;
    const token = _.get(req, 'cookies.dtptoken');

    $API.query('checkPost/' + path, {
      token
    })
      .then((data) => {
        if (!data.ok) {
          throw data;
        }

        return data.result;
      })
      .then(async (data) => {
        if (data.exists) {
          return res.redirect('/' + data.short_id);
        }

        if (!_.get(req, 'session.user.id')) {
          return res.redirect('http://telegra.ph/' + path);
        }

        const page = await $Pages.render('telegraph-publish', {
          check: data,
          session: req.session,
          path
        });

        res.end(page);
      })
      .catch((err) => {
        const apiError = _.get(err, 'error.code');
        
        if (apiError === 3 || apiError === 5) {
          return res.redirect('http://telegra.ph/' + path);
        } 
        
        console.log(err);
        res.status(502).end('Unknown error...');
      })
  });
};