const $Pages = require('../modules/pages');

module.exports = function (app) {
  app.get('/:username', (req, res, next) => {
    $Pages.render('author-page', {
      cookies: req.cookies,
      session: req.session,
      username: req.params.username
    })
      .then((data) => {
        res.end(data);
      })
      .catch((err) => {
        if (err === 'INVALID_USERNAME') {
          return next();
        }

        console.log(err);
        res.status(502).end('Unknown error...');
      })
  });
};