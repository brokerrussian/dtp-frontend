const $API = require('../modules/api');
const $log = require('../libs/log');
const $Session = require('../modules/session');

module.exports = function (app) {
  app.get('/logout', (req, res) => {
    const token = req.cookies.dtptoken;
    
    res.clearCookie('dtptoken');

    $API.query('revokeToken', {
      token
    })
      .then(async () => {
        await $Session.delete(token);
        res.redirect('/');
      })
      .catch((err) => {
        $log.error(err);
        res.end('Server Error!');
      });
  });
};