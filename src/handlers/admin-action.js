const $log = require('../libs/log');

const checkAdmin = require('../middlewares/check-admin');
const session = require('../middlewares/session');

module.exports = function (app) {
  app.post('/ac/:action', checkAdmin, (req, res) => {
    const action = req.params.action;
    let component;

    try {
      component = require('../admin-action/' + action);
    } catch (err) {
      component = false;
    }

    if (!component) {
      return res.status(404).end('Component is not found');
    }

    component(req)
      .then((result) => {
        if (typeof result === 'object') {
          res.json(result);
        } else {
          res.end(result);
        }
      })
      .catch((err) => { 
        $log.error(err);
        res.end('Server Error!');
      });
  });
};