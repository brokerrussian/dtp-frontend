const $Pages = require('../modules/pages');

const checkAdmin = require('../middlewares/check-admin');

module.exports = function (app) {
  app.get('/pdk', checkAdmin, (req, res) => {
    if (!req.session) {
      return res.redirect('/');
    }

    $Pages.render('cpanel', {
      session: req.session
    })
      .then((data) => {
        res.end(data);
      })
      .catch((err) => {
        console.log(err);
        res.status(502).end('Unknown error...');
      });
  });
};