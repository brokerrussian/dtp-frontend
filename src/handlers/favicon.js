const $path = require('path')
const iconPath = $path.resolve(__dirname, '../client/images/favicon2.ico');

module.exports = function (app) {
  app.get('/favicon.ico', (req, res) => {
    res.sendFile(iconPath);
  });
};