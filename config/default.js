const PORT = require('../../dtp-ecosystem/reference/port');
const ENV = process.env.NODE_ENV;

module.exports = {
  server: {
    middlewares: require('./system/middlewares'),
    handlers: require('./system/handlers'),
    port: PORT['dtp-front'][ENV]
  },
  services: {
    api: {
      url: 'http://127.0.0.1:' + PORT['dtp-api'][ENV]
    },
    publisherBot: {
      username: ''
    },
    files: {
      url: ''
    },
    dtp: {
      url: ''
    },
    shortHost: {
      url: ''
    },
    admin: {
      url: 'http://127.0.0.1:' + PORT['dtp-admin'][ENV]
    },
    cgnode: {
      url: ''
    }
  },
  mongo: {
    db: 'DTP-Database',
    models: require('../../dtp-api/config/system/mongo-models'),
    strict: true,
    log: 'info'
  }
};